#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <string>

/**
 * Main function.
 */


#include <vtkPolyData.h>  
#include <vtkSmartPointer.h> 
#include <vtkMassProperties.h>  
#include <vtkPolyDataReader.h>   
  
int main( int argc, char* argv[] ){  
  
    // read in the .vtk file  
    vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New();  
    reader->SetFileName(argv[1]);  
    reader->Update();  
  
    vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();  
    polyData = reader->GetOutput();  
  
    // evaluate the volume  
    vtkMassProperties *mass = vtkMassProperties::New();  
  
    mass->SetInputData(polyData);  
  
    cout << "---- the volume of this vtk polydata is: " << mass->GetVolume() << endl;  
    cout << "---- the volume projected of this vtk polydata is: " << mass->GetVolumeProjected() << endl; 
    cout << "---- the surface area of this vtk polydata is: " << mass->GetSurfaceArea() << endl;  

    return EXIT_SUCCESS;  
} 
