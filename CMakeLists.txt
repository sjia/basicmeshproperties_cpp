cmake_minimum_required(VERSION 3.2)

project (BasicCal)

ADD_DEFINITIONS(
    -std=c++11
    -fpermissive
)

find_package(VTK)
if(VTK_FOUND)
   include(${VTK_USE_FILE})
endif(VTK_FOUND)
add_executable(BasicCal BasicCal.cpp)
TARGET_LINK_LIBRARIES(BasicCal ${VTK_LIBRARIES})